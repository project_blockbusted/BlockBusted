import os
import pymongo

dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
mongo_str = f"mongodb://{dbuser}:{dbpass}@{dbhost}"

client = pymongo.MongoClient(mongo_str)
db = client[dbname]
db.users.create_index([("username", pymongo.ASCENDING)], unique=True)


class UserQueries:
    def get_all_users(self):
        db = client[dbname]
        result = list(db.users.find())
        for value in result:
            value["id"] = value["_id"]
        return result

    def get_user(self, id):
        db = client[dbname]
        result = db.users.find_one({"_id": id})
        if result:
            result["id"] = result["_id"]
        return result

    def create_user(self, data):
        db = client[dbname]
        collection = db.users
        user = collection.find_one({"email": "<user-email>"})
        if user:
            raise Exception("Email already exists!")
        result = db.users.insert_one(data.dict())
        if result.inserted_id:
            result = self.get_user(result.inserted_id)
            result["id"] = str(result["id"])
        return result
