import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import movies, auth, accounts


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(movies.router, tags=['Movies'])
app.include_router(auth.authenticator.router, tags=['Accounts'])
app.include_router(accounts.router, tags=['Accounts'])
