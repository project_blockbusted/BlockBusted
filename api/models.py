from bson.objectid import ObjectId
from pydantic import BaseModel, Field
from typing import List


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError as e:
                raise ValueError(f"Not a valid object id: {value}") from e
        return value


class SessionOut(BaseModel):
    jti: str
    account_id: str


class AccountIn(BaseModel):
    username: str
    email: str
    password: str


class Account(AccountIn):
    id: PydanticObjectId
    roles: List[str]


class AccountOut(BaseModel):
    id: str
    email: str
    username: str
    roles: List[str]
    watchlist: List[str] = Field(default_factory=list)


class MovieIn(BaseModel):
    movie_title: str
    image_url: str
    genre: str
    release_date: str
    rating: str
    synopsis: str
    tconst: str


class Movie(MovieIn):
    id: PydanticObjectId


class MovieOut(MovieIn):
    id: str


class MovieList(BaseModel):
    movies: List[MovieOut]


class MovieScroll(BaseModel):
    title: str
    poster: str


class WatchlistIn(BaseModel):
    tconst: str


class WatchlistOut(BaseModel):
    movies: List[MovieOut]
