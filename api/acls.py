import requests
import os
import json


X_RapidAPI_Key = os.environ["X_RapidAPI_Key"]


def get_movie_streaming_details(tconst):
    headers = {
        "X-RapidAPI-Key": X_RapidAPI_Key,
        "X-RapidAPI-Host": "streaming-availability.p.rapidapi.com",
        "content-type": "application/json",
    }

    url = "https://streaming-availability.p.rapidapi.com/get/basic"
    querystring = {"country": "us", "imdb_id": tconst, "output_language": "en"}
    response = requests.get(url, headers=headers, params=querystring)
    if response.status_code == 200:
        movie_streaming_details = json.loads(response.text)
        return movie_streaming_details
    else:
        return None


def get_movie_details(tconst):
    headers = {
        "X-RapidAPI-Key": X_RapidAPI_Key,
        "X-RapidAPI-Host": "online-movie-database.p.rapidapi.com",
        "content-type": "application/json",
    }
    url1 = (
        "https://online-movie-database.p.rapidapi.com/title/get-overview-details"
    )
    querystring = {"tconst": tconst, "r": "json"}
    response = requests.get(url1, headers=headers, params=querystring)
    movie_details = json.loads(response.text)
    print(movie_details)
    return movie_details


def get_movie_list():
    url = (
        "https://online-movie-database.p.rapidapi.com/title/get-most-popular-movies"
    )
    headers = {
        "X-RapidAPI-Key": X_RapidAPI_Key,
        "X-RapidAPI-Host": "online-movie-database.p.rapidapi.com",
        "content-type": "application/json",
    }
    querystring = {
        "currentCountry": "US",
        "purchaseCountry": "US",
        "homeCountry": "US",
    }
    response = requests.request(
        "GET", url, headers=headers, params=querystring
    )
    list_movies = json.loads(response.text)
    new_list = list_movies[0:15]
    movie_list_details = []
    for movie in new_list:
        url2 = "https://online-movie-database.p.rapidapi.com/title/get-details"
        tconst = movie[7:17]
        querystring = {"tconst": tconst, "r": "json"}
        response = requests.get(url2, headers=headers, params=querystring)
        movie_list_details.append(json.loads(response.text))
    return movie_list_details


def search_movies(title):
    url = "https://online-movie-database.p.rapidapi.com/title/v2/find/"
    querystring = {"title": title, "limit": "15", "r": "json"}
    headers = {
        "X-RapidAPI-Key": X_RapidAPI_Key,
        "X-RapidAPI-Host": "online-movie-database.p.rapidapi.com",
        "content-type": "application/json",
    }
    response = requests.get(url, headers=headers, params=querystring)
    response_json = json.loads(response.text)
    search_movie = response_json["results"]
    return search_movie

def add_to_watchlist(tconst):
    headers = {
        "X-RapidAPI-Key": X_RapidAPI_Key,
        "X-RapidAPI-Host": "online-movie-database.p.rapidapi.com",
        "content-type": "application/json",
    }
    url1 = (
        "https://online-movie-database.p.rapidapi.com/title/get-overview-details"
    )
    querystring = {"tconst": tconst, "r": "json"}
    response = requests.get(url1, headers=headers, params=querystring)
    movie_details = json.loads(response.text)
    return movie_details

# def get_watchlist():
#     url = (
#         "https://online-movie-database.p.rapidapi.com/title/get-overview-details"
#     )
#     headers = {
#         "X-RapidAPI-Key": X_RapidAPI_Key,
#         "X-RapidAPI-Host": "online-movie-database.p.rapidapi.com",
#         "content-type": "application/json",
#     }
#     querystring = {
#         "currentCountry": "US",
#         "purchaseCountry": "US",
#         "homeCountry": "US",
#     }
#     response = requests.request(
#         "GET", MovieQueries, headers=headers, params=querystring
#     )
#     list_movies = json.loads(response.text)
#     print("####################",response.text)
#     new_list = list_movies[0:5]
#     movie_list_details = []
#     for movie in new_list:
#         # url2 = "https://online-movie-database.p.rapidapi.com/title/get-overview-details"
#         tconst = movie[7:17]
#         querystring = {"tconst": tconst, "r": "json"}
#         response = requests.get( headers=headers, params=querystring)
#         movie_list_details.append(json.loads(response.text))
#     return movie_list_details
