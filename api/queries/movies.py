from typing import List
from pymongo import ReturnDocument
from .client import Queries
from bson.objectid import ObjectId
from models import MovieIn, MovieOut, WatchlistIn, WatchlistOut
import requests

from acls import get_movie_details, get_movie_streaming_details


class MovieQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "accounts"

    def add_to_watchlist(self, user_id, tconst):
        record = self.collection.find_one_and_update(
            {"_id": ObjectId(user_id)},
            {"$addToSet": {"watchlist": tconst}},
            upsert=True,
            return_document=ReturnDocument.AFTER,
        )
        if record:
            return {"Successfully added to watchlist!"}

    def get_watchlist(self, user_id):
        record = self.collection.find_one({"_id": ObjectId(user_id)})
        watchlist = []
        for tconst in record["watchlist"]:
            response = get_movie_details(tconst)
            watchlist.append(
                {
                    "title": response["title"]["title"],
                    "image_url": response["title"]["image"]["url"],
                    "id": response["id"]
                }
            )
        return watchlist

    def remove_from_watchlist(
        self, user_id: str, tconst: str
    ) -> WatchlistOut:
        result = self.collection.update_one(
            {"_id": ObjectId(user_id)}, {"$pull": {"watchlist": tconst}}
        )
        return result.modified_count > 0


# class MQueries:
#     def get_movies(self, movie_name: str, token: str) -> MovieOut:
#         params = {
#             "q": movie_name,
#             "type": "movies",
#         }
#         return requests.get(
#             "https://online-movie-database.p.rapidapi.com/title/find?q",
#             params=params,
#             headers=token,
#         ).json()
