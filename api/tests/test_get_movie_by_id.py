from fastapi.testclient import TestClient
from main import app
from queries.movies import MovieQueries
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": 99, "roles": "fakeuser"}


class FakeMoviesQueries:
    def get_all(self):
        return {}


def test_get_all():
    app.dependency_overrides[MovieQueries] = FakeMoviesQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    access_token = "valid_access_token"
    headers = {"Authorization": f"Bearer {access_token}"}
    response = client.get("/movie/tt1234567", headers=headers)
    data = response.json()
    assert response.status_code == 200
    assert isinstance(data, dict)
