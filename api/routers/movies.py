from fastapi import APIRouter, Depends, HTTPException, status
from queries.movies import MovieQueries
from routers.sockets import socket_manager
from authenticator import authenticator
import os
from acls import (
    get_movie_details,
    get_movie_list,
    search_movies,
    get_movie_streaming_details,
    # get_watchlist

)


router = APIRouter()

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

url = "https://movie-database-alternative.p.rapidapi.com/"

headers = {
    "X-RapidAPI-Key": os.environ["X_RapidAPI_Key"],
    "X-RapidAPI-Host": "movie-database-alternative.p.rapidapi.com",
}


@router.get("/movie/{title}")
def get_movie(
    title: str,
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    response = get_movie_details(title)
    if not account or "user" not in account["roles"]:
        raise not_authorized
    return response


@router.get("/search/{title}")
def search_movie(
    title: str,
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    response = search_movies(title)
    if not account or "user" not in account["roles"]:
        raise not_authorized
    return response


@router.get("/movie/title/{tconst}")
def get_movies(
    tconst: str,
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    response = get_movie_details(tconst)
    response2 = get_movie_streaming_details(tconst)
    if not account or "user" not in account["roles"]:
        raise not_authorized
    if not response2:
        return {"resp1": response}
    results = {"resp1": response, "resp2": response2}
    return results


@router.get("/movies")
async def get_movies2(
    repo: MovieQueries = Depends(),
    account=Depends(authenticator.try_get_current_account_data),
):
    if not account or "user" not in account["roles"]:
        raise not_authorized
    response = get_movie_list()
    await socket_manager.broadcast_refetch()
    return response

@router.post("/watchlist/{tconst}")
def add_movie_to_watchlist(
    tconst: str,
    account: dict = Depends(authenticator.try_get_current_account_data),
    repo: MovieQueries = Depends(),
):
    if not account or "user" not in account["roles"]:
        raise not_authorized
    print(tconst)
    repo.add_to_watchlist(account["id"], tconst)
    return {"message": "Movie added to watchlist"}


@router.get("/watchlist")
async def get_watchlist(
    repo: MovieQueries = Depends(),
    account=Depends(authenticator.try_get_current_account_data),
):
    if not account or "user" not in account["roles"]:
        raise not_authorized
    response = repo.get_watchlist(account["id"])
    await socket_manager.broadcast_refetch()
    return response

@router.delete("/watchlist/{tconst}")
async def delete_movie_from_watchlist(
    tconst: str,
    account: dict = Depends(authenticator.try_get_current_account_data),
    repo: MovieQueries = Depends(),
):
    if not account or "user" not in account["roles"]:
        raise not_authorized
    print(tconst)
    repo.remove_from_watchlist(account["id"], tconst)


