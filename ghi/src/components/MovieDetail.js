
import React, { useState, useEffect } from 'react';
import { useAuthContext } from "../AuthProvider";
import { useParams } from "react-router-dom";
import "../styles/MovieList.css";
import netflix from "../assets/netflix.png";
import hulu from "../assets/hulu.jpg";
import hbo from "../assets/hbo max.jpg";
import disney from "../assets/disney plus.jpg";
import paramount from "../assets/paramountplus.jpg";
import prime from "../assets/prime3.png";
import starz from "../assets/starz.png";
import showtime from "../assets/showtime.jpg";
import peacock from "../assets/peacock.jpg";

function MovieDetails() {
    const { token } = useAuthContext();
    const [movie, setMovie] = useState();
    const [streamingData, setStreamingData] = useState(false);
    const { movie_id } = useParams();
    const [loading, setLoading] = useState(false);
    const [isInWatchlist, setIsInWatchlist] = useState(false);
    const [animationClass, setAnimationClass] = useState({
        watchlist: "fade-in-watchlist",
        watchlist2: "fade-in-watchlist2",
    });

    const fetchData = async () => {
        try {
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/movie/title/${movie_id}/`;
            setLoading(true);

            const response = await fetch(url, {
                Authorization: `Bearer ${token}`,
                credentials: "include",
            });
            if (response.ok) {
                const data = await response.json();
                setMovie(data.resp1);
                setStreamingData(data.resp2);
                setLoading(false);
            } else if (response.status === 401) {
                window.location.href = "/";
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();

    }, []);

    const handleWatchlist = async (event) => {
        event.preventDefault();

        try {
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/watchlist/${movie_id}`;

            const response = await fetch(url, {
                method: "POST",
                Authorization: `Bearer ${token}`,
                credentials: 'include',
                body: movie_id,
            });
            const storedWatchlist = sessionStorage.getItem('watchlist');
            if (storedWatchlist) {
                const parsedWatchlist = JSON.parse(storedWatchlist);
                parsedWatchlist.push(movie_id);
                sessionStorage.setItem('watchlist', JSON.stringify(parsedWatchlist));
            } else {
                sessionStorage.setItem('watchlist', JSON.stringify([movie_id]));
            }

            setIsInWatchlist(true);
            setAnimationClass((prevState) => ({
                ...prevState,
                watchlist: "fade-out-watchlist",
                watchlist2: "fade-in-watchlist2",
            }));
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    };


    const handledelete = async (event) => {
        event.preventDefault();
        try {
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/watchlist/${movie_id}`;
            const response = await fetch(url, {
                method: "DELETE",
                Authorization: `Bearer ${token}`,
                credentials: 'include',
                body: movie_id
            })
            const storedWatchlist = sessionStorage.getItem('watchlist');
            if (storedWatchlist) {
                const parsedWatchlist = JSON.parse(storedWatchlist);
                const index = parsedWatchlist.indexOf(movie_id);
                if (index > -1) {
                    parsedWatchlist.splice(index, 1);
                    sessionStorage.setItem('watchlist', JSON.stringify(parsedWatchlist));
                }
            }

            setIsInWatchlist(false)
            setAnimationClass((prevState) => ({
                ...prevState,
                watchlist: "fade-in-watchlist",
                watchlist2: "fade-out-watchlist",
            }));
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        const storedWatchlist = sessionStorage.getItem('watchlist');
        if (storedWatchlist) {
            const parsedWatchlist = JSON.parse(storedWatchlist);
            setIsInWatchlist(parsedWatchlist.includes(movie_id));
        }
    }, []);

    if (loading) {
        return (
            <div>
                <div className="loading" >
                    <div className="loading-text">
                        <span className="loading-text-words">L</span>
                        <span className="loading-text-words">O</span>
                        <span className="loading-text-words">A</span>
                        <span className="loading-text-words">D</span>
                        <span className="loading-text-words">I</span>
                        <span className="loading-text-words">N</span>
                        <span className="loading-text-words">G</span>
                    </div>
                </div>
            </div>
        )
    }

    if (movie) {
        const movieDetailsStyle = {
            backgroundColor: 'black',
            opacity: 0.75,
            padding: '20px',
            borderRadius: '30px',
            maxWidth: '50%',
            margin: 'auto',
            fontSize: '24px'
        };

        const containerStyle = {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        };

        const movieImageStyle = {
            maxWidth: '50%',
            maxHeight: '50%',
            marginRight: '20px'
        };

        return (
            <div>
                <div className="blur-background" style={{ backgroundImage: `url(${movie.title.image.url})`, filter: 'blur(10px)', height: '150vh', width: '100%', backgroundSize: 'cover', backgroundPosition: 'center', position: 'absolute', zIndex: '-1' }}></div>
                <>
                    <div style={{ backgroundColor: 'rgba(0,0,0,0.0)', padding: '10px' }}>
                        <h1 className="title3 bottomtext2" style={{ color: 'white', margin: 0, paddingLeft: '225px' }}>{movie.title.title}</h1>

                    </div>
                    <div style={containerStyle}>
                        <img src={movie.title.image.url} className="img-thumbnail" alt="movie image" height="1000" width="500" style={movieImageStyle} />
                        <div style={movieDetailsStyle}>
                            <p style={{ color: 'white' }}> Year : {movie.title.year}</p>
                            <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} />
                            {movie.certificates && <>
                                <p style={{ color: 'white' }}> Rated : {movie.certificates.US[0].certificate} </p>
                                <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} /></>}
                            <p style={{ color: 'white' }}> Genre : {movie.genres[0]} </p>
                            <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} />
                            <p style={{ color: 'white' }}> IMDB Rating : {movie.ratings.rating} </p>
                            <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} />
                            {movie.title.runningTimeInMinutes ? <>
                                <p style={{ color: 'white' }}> Run time : {movie.title.runningTimeInMinutes} minutes </p>
                                <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} />
                            </>
                                : null}
                            {streamingData ? <>
                                <p style={{ color: 'white' }}>{streamingData.overview} </p>
                                <hr style={{ backgroundColor: 'white', height: '1px', border: 'none', margin: '5px 0' }} />
                                <div className="streaminglogo">
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.hulu ? <a href={streamingData.streamingInfo.hulu.us.link} target="_blank"><img src={hulu} alt="Hulu icon" width="80" height="50" /> </a> : null}
                                    </div>

                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.netflix ? <a href={streamingData.streamingInfo.netflix.us.link} target="_blank"><img src={netflix} alt="Netflix icon" width="80" height="50" /></a> : null}
                                    </div>

                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.disney ? <a href={streamingData.streamingInfo.disney.us.link} target="_blank"><img src={disney} alt="Disney icon" width="80" height="50" /></a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.prime ? <a href={streamingData.streamingInfo.prime.us.link} target="_blank"><img src={prime} alt="Prime icon" width="80" height="30" /></a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.hbo ? <a href={streamingData.streamingInfo.hbo.us.link} target="_blank"><img src={hbo} alt="Hbo icon" width="100" height="50" /></a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.starz ? <a href={streamingData.streamingInfo.starz.us.link} target="_blank"><img src={starz} alt="Starz icon" width="80" height="50" /> </a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.paramount ? <a href={streamingData.streamingInfo.paramount.us.link} target="_blank"><img src={paramount} alt="Paramount icon" width="150" height="100" /></a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.showtime ? <a href={streamingData.streamingInfo.showtime.us.link} target="_blank"><img src={showtime} alt="Showtime icon" width="170" height="100" /></a> : null}
                                    </div>
                                    <div style={{ display: 'inline-block', margin: '5px' }}>
                                        {streamingData.streamingInfo.peacock ? <a href={streamingData.streamingInfo.peacock.us.link} target="_blank"><img src={peacock} alt="Peacock icon" width="170" height="100" /></a> : null}
                                    </div>
                                </div>
                                <div className="watchlists">
                                    <div style={{ display: 'inline-block' }}>
                                    </div>
                                    <div>
                                        {!isInWatchlist && (
                                            <button className={`button-watchlist ${animationClass.watchlist}`} onClick={handleWatchlist}>
                                                Add to Watchlist
                                            </button>
                                        )}
                                        {isInWatchlist && (
                                            <button className={`button-watchlist2 ${animationClass.watchlist2}`} onClick={handledelete}>Remove From Watchlist</button>
                                        )}
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </>
                                : null}
                        </div>
                    </div>
                </>
            </div>
        );
    }
};
export default MovieDetails;
