import React from 'react';
import '../styles/profiles.css';
import spotlight from "../assets/Shimmy.mp4";
import yan from "../assets/YAN.JPG";
import lyndon from "../assets/lyndon.jpg";
import anton from "../assets/anton.jpeg";
import celeste from "../assets/celeste.jpeg";


function ProfileCard() {
    return (
        <div>
            <h1 className="bottomtext2" style={{ fontSize: '4rem', textAlign: 'center', margin: 0, padding: '20px' }}> Developers</h1>
            <div className="container">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                </link>
                <div className="row">
                    <div className="col-md-3">
                        <div className="profile-card card-1 ">
                            <img src={yan} alt="team profile" />
                            <div className="profile-content">
                                <h3>Yannis Pashalidis</h3>
                                <p>Experienced and skilled full-stack developer proficient in Python, JavaScript, React, and Django with knowledge in Docker, MongoDB, and CSS. Passionate about creating efficient software solutions and user-friendly interfaces. Always eager to learn and stay up-to-date with the latest trends and technologies in the industry.</p>
                                <ul>
                                    <li><a href="" target="_blank" className="fa fa-facebook"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-twitter"></a></li>
                                    <li><a href="https://www.linkedin.com/in/yannis-pashalidis" target="_blank" className="fa fa-linkedin"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-instagram"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-gitlab"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="profile-card card-2 ">
                            <img src={lyndon} alt="team profile" />
                            <div className="profile-content">
                                <h3>Lyndon Hanson</h3>
                                <p>Full Stack Software Engineer | Developer | JavaScript (ES6) | Python | React.js | Node.js | MongoDB | Docker | Django | FastAPI | HTML5 | CSS.  I enjoy consistently being challenged and I enjoy consistently learning about software development.  I love being outside, playing video games, and cooking in my free time</p>
                                <ul>
                                    <li><a href="" target="_blank" className="fa fa-facebook"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-twitter"></a></li>
                                    <li><a href="https://www.linkedin.com/in/lyndon-hanson-iv/" target="_blank" className="fa fa-linkedin"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-instagram"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-gitlab"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="profile-card card-3">
                            <img src={anton} alt="team profile" />
                            <div className="profile-content">
                                <h3>Anton DeCesare</h3>
                                <p>I'm a software engineer skilled in JavaScript, Python, React, Postgres, SQL, and Django. I specialize in writing, testing, and maintaining code for web applications. I'm passionate about developing complex and dynamic software systems using React and Django. I have experience working with relational databases such as Postgres and SQL, which helps me create efficient software solutions. My ultimate goal is to continue learning and improving my skills to build innovative software applications.</p>
                                <ul>
                                    <li><a href="https://www.linkedin.com/in/anton-decesare/" target="_blank" className="fa fa-linkedin"></a></li>
                                    <li><a href="https://gitlab.com/antonpdecesare" target="_blank" className="fa fa-gitlab"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="profile-card card-4">
                            <img src={celeste} alt="team profile" />
                            <div className="profile-content">
                                <h3>Celeste Villar</h3>
                                <p>Fullstack Developer - Python, JavaScript, React, FastAPI , Docker, Django, MongoDB, and CSS.</p>
                                <ul>
                                    <li><a href="" target="_blank" className="fa fa-facebook"></a></li>
                                    <li><a href="" target="_blank" className="fa fa-twitter"></a></li>
                                    <li><a href="https://www.linkedin.com/in/celeste-villar/" target="_blank" className="fa fa-linkedin"></a></li>
                                    <li><a href="https://www.instagram.com/cvillar510/" target="_blank" className="fa fa-instagram"></a></li>
                                    <li><a href="https://gitlab.com/Celeste_Villar" target="_blank" className="fa fa-gitlab"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="main main2">
                        <video
                            style={{ position: "fixed", zIndex: -1, width: "100%", height: "100%", filter: `blur(10px)` }}
                            src={spotlight}
                            autoPlay
                            loop
                            muted
                        />
                    </div>
                </div>
            </div>
        </div>

    );
}

export default ProfileCard;
