import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Alert from "react-bootstrap/Alert";
import Form from "react-bootstrap/Form";
import { NavLink, Link } from "react-router-dom";
import { useState, useEffect } from "react";
import '../styles/nav.css';


function LoginModal() {
    const [show, setShow] = useState(false);
    const [alertShow, setAlertShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [formData, setFormData] = useState({
        username: "",
        password: "",
    });

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`;
        const form = new FormData();
        form.append("username", formData.email);
        form.append("password", formData.password);
        const response = await fetch(url, {
            method: "POST",
            credentials: "include",
            body: form,
        });

        if (response.ok) {
            handleClose();
            window.location.reload();
            return;
        } else {
            setAlertShow(true);
        }
    };
    return (
        <>
            <div onClick={handleShow} className="bottomtext3">Sign In</div>
            <Modal show={show} onHide={handleClose} className="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Sign In</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group
                            className="mb-3"
                            controlId="SignInForm.ControlInput1"
                            onChange={handleFormChange}
                        >
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" type="text" autoFocus />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="SignInForm.ControlInputText1"
                            onChange={handleFormChange}
                        >
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" autoFocus />
                        </Form.Group>
                        {alertShow && (
                            <Alert variant={"danger"}>Invalid Email or Password</Alert>
                        )}
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="secondary" onClick={handleSubmit}>
                        Sign In
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
function SignupModal() {
    const [signupFail, setSignupFail] = useState(false);
    const [characterFail, setCharacterFail] = useState(false);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [formData, setFormData] = useState({
        email: "",
        password: "",
        username: "",
    });
    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (Object.values(formData).includes("")) {
            setCharacterFail(true);
        } else {
            try {
                let url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/accounts`;
                let response = await fetch(url, {
                    method: "POST",
                    body: JSON.stringify(formData),
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                if (response.status === 200) {
                    setShow(false);
                    let url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`;
                    let form = new FormData();
                    form.append("username", formData.email);
                    form.append("password", formData.password);
                    let response = await fetch(url, {
                        method: "POST",
                        credentials: "include",
                        body: form,
                    });
                    if (response.ok) {
                        window.location.reload();
                        handleClose();
                        return;
                    }
                }
            } catch (error) {
                setSignupFail(true);
            }
        }
    };
    return (
        <>
            <div onClick={handleShow} className="bottomtext3">Sign Up</div>

            <Modal show={show} onHide={handleClose} className="custom-modal">
                <Modal.Header closeButton>
                    <Modal.Title>Sign Up</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group
                            className="mb-3"
                            controlId="SignUpForm.ControlInput1"
                            onChange={handleFormChange}
                        >
                            <Form.Label>UserName</Form.Label>
                            <Form.Control name="username" type="text" autoFocus />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="SignUpForm.ControlInput2"
                            onChange={handleFormChange}
                        >
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" type="text" autoFocus />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="SignUpForm.ControlInput3"
                            onChange={handleFormChange}
                        >
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" autoFocus />
                        </Form.Group>
                        {signupFail && (
                            <Alert variant={"danger"}>This already exists</Alert>
                        )}
                        {characterFail && <Alert variant={"danger"}>No empty fields</Alert>}
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="secondary" onClick={handleSubmit}>
                        Sign Up
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
function Nav() {
    const [token, setToken] = useState("");
    const [loginModalShow, setLoginModalShow] = useState(false);
    const [signupModalShow, setSignupModalShow] = useState(false);

    useEffect(() => {
        async function getData() {
            const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`;
            const response = await fetch(url, {
                credentials: "include",
            });
            if (response.ok) {
                const data = await response.json();
                setToken(data);
            }
        }
        getData();
    }, []);

    const handleLogout = async () => {
        if (token) {
            const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`;
            await fetch(url, { method: "delete", credentials: "include" });
            setToken(null);
            window.location.reload();
        }
    };

    return (
        <>
            {token ? (
                <header>
                    <nav>
                        <div className="welcome bottomtext4">Welcome  {token.account.username}!</div>
                        <Link to={"/"} style={{ textDecoration: "none" }}>
                            <div className="logo bogo">
                                <p >
                                    <span>B</span>
                                    <span>L</span>
                                    <span>O</span>
                                    <span>C</span>
                                    <span>K</span>
                                    <span>B</span>
                                    <span>U</span>
                                    <span>S</span>
                                    <span>T</span>
                                    <span>E</span>
                                    <span>D</span>
                                </p>
                            </div>
                        </Link>
                        <div className="menu bottomtext3">
                            <NavLink to={"/watchlist"} style={{ textDecoration: "none" }}>
                                <div>Watchlist</div>
                            </NavLink>
                            <NavLink to={"/movies"} style={{ textDecoration: "none" }}>
                                <div>Trending</div>
                            </NavLink>
                            <NavLink to={"/search"} style={{ textDecoration: "none" }}>
                                <div>Search</div>
                            </NavLink >
                            <NavLink to={"/developers"} style={{ textDecoration: "none" }}>
                                <div>About Us</div>
                            </NavLink >
                            <NavLink to={"/"} style={{ textDecoration: "none" }}>
                                <div onClick={handleLogout}>Logout</div>
                            </NavLink >
                        </div>
                    </nav>
                </header>
            ) : (
                <>
                    <header>

                        <nav>

                            <Link to={"/"} style={{ textDecoration: "none" }}>
                                <div className="logo bogo">
                                    <p >
                                        <span>B</span>
                                        <span>L</span>
                                        <span>O</span>
                                        <span>C</span>
                                        <span>K</span>
                                        <span>B</span>
                                        <span>U</span>
                                        <span>S</span>
                                        <span>T</span>
                                        <span>E</span>
                                        <span>D</span>
                                    </p>
                                </div>
                            </Link>
                            <div className="menu1">
                                <LoginModal
                                    show={loginModalShow}
                                    onHide={() => setLoginModalShow(false)}
                                ></LoginModal>
                                <SignupModal
                                    show={signupModalShow}
                                    onHide={() => setSignupModalShow(false)}
                                ></SignupModal>
                            </div>
                        </nav>
                    </header>
                </>
            )}
        </>
    );
}

export default Nav;
