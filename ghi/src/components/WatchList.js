import React, { useState, useEffect } from 'react';
import { useAuthContext } from '../AuthProvider';
import { useNavigate } from 'react-router-dom';
import { Card, Button, Carousel, Row, Col } from 'react-bootstrap';
import '../styles/MovieList.css';
import particle from "../assets/Sequence6.mp4";

function Watchlist() {
    const { token } = useAuthContext();
    const [watchlist, setWatchlist] = useState([]);
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const fetchData = async () => {
        try {
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/watchlist`;
            setLoading(true);
            const response = await fetch(url, {
                "Authorization": `Bearer ${token}`,
                "credentials": "include"
            });

            if (response.ok) {
                const data = await response.json();
                setWatchlist(data);
                console.log(data);
                setLoading(false);
            } else if (response.status === 401) {
                window.location.href = "/";
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    const movieChunks = watchlist.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 3);
        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = [];
        }
        resultArray[chunkIndex].push(item);
        if(resultArray){
            return resultArray;
        }else{
            return null
        }
    }, []);

    return (
        <div>
            {loading ? (
                <div className="loading" >
                    <div className="loading-text">
                        <span className="loading-text-words">L</span>
                        <span className="loading-text-words">O</span>
                        <span className="loading-text-words">A</span>
                        <span className="loading-text-words">D</span>
                        <span className="loading-text-words">I</span>
                        <span className="loading-text-words">N</span>
                        <span className="loading-text-words">G</span>
                    </div>
                </div>
            ) : (
                    <div>
                        <h1 className="bottomtext2" style={{ fontSize: '4rem', textAlign: 'center', margin: 0, padding: '20px' }}>My Watchlist</h1>
                        {watchlist.length === 0 ? (
                            <div>
                                <h2 style={{ textAlign: 'center', marginTop: '400px', color: 'grey', fontSize: '60px', textShadow: '4px 4px 4px rgba(0, 0, 0, 0.8)' }}>No Movies/Shows in Watchlist</h2>
                            </div>
                        ) : (
                            <Carousel interval={null}>
                                {movieChunks.map((chunk, chunkIndex) => {
                                    return (
                                        <Carousel.Item key={chunkIndex}>
                                            <Row>
                                                {chunk.map((movie) => {
                                                    return (
                                                        <Col key={movie.title} className="mb-4">
                                                            {movie.image_url && (
                                                                <Card>
                                                                    <div className="container">
                                                                        <Card.Img className="movie-card blur" style={{ backgroundImage: `url(${movie.image_url})` }} />
                                                                    </div>
                                                                    <Card.ImgOverlay>
                                                                        <img src={movie.image_url} className="img-thumbnail" alt="movie image" />
                                                                        <Card.Title className="titlecard">{movie.title}</Card.Title>
                                                                        <Button
                                                                            className="buttondetail"
                                                                            variant="black"
                                                                            size="lg"
                                                                            onClick={() => navigate(`/movie${movie.id}`)}
                                                                        >
                                                                            Details
                                                                        </Button>
                                                                    </Card.ImgOverlay>
                                                                </Card>
                                                            )}
                                                        </Col>
                                                    );
                                                })}
                                            </Row>
                                        </Carousel.Item>
                                    );
                                })}
                            </Carousel>
                        )}
                        <div className="main main5">
                            <video
                                style={{ position: "fixed", zIndex: -1, width: "100%", height: "100%" }}
                                src={particle}
                                autoPlay
                                loop
                                muted
                            />
                        </div>
                    </div>
                        )}
                    </div>

    );

}

export default Watchlist;