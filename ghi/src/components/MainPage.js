import React, { useEffect, useState } from 'react';
import { Container } from "react-bootstrap";
import batmane from "../assets/batmane.mp4";
import "../styles/index.css";

function MainPage() {
  const [fadeIn, setFadeIn] = useState(false);

  useEffect(() => {
    setFadeIn(true);
  }, []);

  return (
    <>
      <div className="main main2" style={{ position: "fixed", top: 0, left: 0, width: "100%", height: "100%", zIndex: -1 }}>
        <video style={{ objectFit: "cover", width: "100%", height: "100%", filter: "brightness(0.6)" }} src={batmane} autoPlay loop muted />
      </div>
      <Container fluid className={`px-5 py-2 my-5 text-light position-absolute`} id="main-view" style={{ zIndex: 1, textAlign: "center" }}>
        <div className="box">
          <p className="neon-text">
            <span>B</span>
            <span>L</span>
            <span>O</span>
            <span>C</span>
            <span>K</span>
            <span>B</span>
            <span>U</span>
            <span>S</span>
            <span>T</span>
            <span>E</span>
            <span>D</span>
          </p>
        </div>
        <p className="bottomtext" style={{ fontSize: "40px", position: "absolute", top: "60%", left: "50%", transform: "translate(-50%, -50%)" }}>THE PREMIER ENTERTAINMENT SEARCH SERVICE</p>
      </Container>
    </>
  );
}

export default MainPage;
