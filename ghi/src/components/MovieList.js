import { useState, useEffect } from 'react';
import { useAuthContext } from '../AuthProvider';
import { useNavigate } from 'react-router-dom';
import { Card, Button, Carousel, Row, Col } from 'react-bootstrap';
import '../styles/MovieList.css';


import particle from "../assets/goldies.mp4";


function MovieList() {
    const { token } = useAuthContext();
    const [movieList, setList] = useState([]);
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);


    const fetchData = async () => {
        try {
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/movies`;
            setLoading(true);
            const response = await fetch(url, {
                Authorization: `Bearer ${token}`,
                credentials: 'include',
            });
            if (response.ok) {
                const data = await response.json();
                setLoading(false);
                setList(data);
            } else if (response.status === 401) {
                setLoading(false);
                window.location.href = "/";
            }
        } catch (error) {
            console.error(error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const movieChunks = movieList.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 3);
        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = [];
        }
        resultArray[chunkIndex].push(item);
        return resultArray;
    }, []);
    return (
        <div>
            {loading ? (
                <div className="loading" >
                    <div className="loading-text">
                        <span className="loading-text-words">L</span>
                        <span className="loading-text-words">O</span>
                        <span className="loading-text-words">A</span>
                        <span className="loading-text-words">D</span>
                        <span className="loading-text-words">I</span>
                        <span className="loading-text-words">N</span>
                        <span className="loading-text-words">G</span>
                    </div>
                </div>
            ) : (
                <div>
                        <h1 className="bottomtext2" style={{ fontSize: '4rem', textAlign: 'center', margin: 0, padding: '20px' }}>Trending Movies</h1>

                    <Carousel interval={null}>
                        {movieChunks.map((chunk, chunkIndex) => {
                            return (
                                <Carousel.Item key={chunkIndex}>
                                    <Row>
                                        {chunk.map((movie) => {
                                            return (
                                                <Col key={movie.id} className="mb-4">
                                                    {movie.image &&
                                                        <Card>
                                                            <div className="container">
                                                                <Card.Img className="movie-card blur" style={{ backgroundImage: `url(${movie.image.url})` }} />
                                                            </div>
                                                            <Card.ImgOverlay >
                                                                <img src={movie.image.url} className="img-thumbnail" alt="movie image" />
                                                                <Card.Title className="titlecard">{movie.title}</Card.Title>
                                                                <Button
                                                                    className="buttondetail"
                                                                    variant="black"
                                                                    size="lg"
                                                                    onClick={() => navigate(`/movie${movie.id}`)}
                                                                >
                                                                    Details
                                                                </Button>
                                                            </Card.ImgOverlay>
                                                        </Card>
                                                    }
                                                </Col>
                                            );
                                        })}
                                    </Row>
                                </Carousel.Item>
                            );
                        })}
                    </Carousel>
                    <div className="main main5">
                        <video
                            style={{ position: "fixed", zIndex: -1, width: "100%", height: "100%" }}
                            src={particle}
                            autoPlay
                            loop
                            muted
                        />
                    </div>
                </div>
            )}
        </div>
    );
}
export default MovieList;
