import React, { useState, useEffect } from 'react';
import { useAuthContext } from "../AuthProvider";
import { useNavigate } from "react-router-dom"
import { Card, Button, Carousel, Row, Col, Form } from 'react-bootstrap';
import '../styles/MovieList.css';
import particle from "../assets/Sequence5.mp4";
import search from "../assets/search.png";

function SearchBar() {
    const { token } = useAuthContext();
    const navigate = useNavigate();
    const [searchQuery, setSearchQuery] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [loading, setLoading] = useState(false);

    const handleSearchChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const handleSearchSubmit = async (event) => {
        try {
            event.preventDefault();
            const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/search/${searchQuery}`
            setLoading(true);
            const response = await fetch(url, {
                "Authorization": `Bearer ${token}`,
                "credentials": "include"
            });
            if (response.ok) {
                const data = await response.json();
                setLoading(false);
                setSearchResults(data);
            } else if (response.status === 401) {
                setLoading(false);
                window.location.href = "/";
            }
        } catch (error) {
            console.error(error);
            setLoading(false);
        }
    }
    useEffect(() => {
    }, []);

    const movieChunks = searchResults.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 3);

        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = [];
        }

        resultArray[chunkIndex].push(item);

        return resultArray;
    }, []);

    return (
        <div>
            {loading ? (
                <div className="loading">
                    <div className="loading-text">
                        <span className="loading-text-words">L</span>
                        <span className="loading-text-words">O</span>
                        <span className="loading-text-words">A</span>
                        <span className="loading-text-words">D</span>
                        <span className="loading-text-words">I</span>
                        <span className="loading-text-words">N</span>
                        <span className="loading-text-words">G</span>
                    </div>
                </div>
            ) : (
                <div>
                    <Row>
                            <Col md={2} className={searchResults.length > 0 ? 'mr-auto' : 'mx-auto text-center'}>
                                <div className="search-bar-container">
                                    <Form onSubmit={handleSearchSubmit} className="search-bar-form">
                                        <div className="search-input-container">
                                            <Form.Control type="text" value={searchQuery} onChange={handleSearchChange} placeholder="Search movies..." className="search-bar-input" />
                                            <button type="submit" className="search-button">
                                                <img src={search} alt="Search" />
                                            </button>
                                        </div>
                                    </Form>
                                </div>
                                <div className={`search-results ${searchResults.length > 0 ? 'show' : ''}`}>
                                </div>
                            </Col>

                    </Row>

                    {searchResults.length > 0 ? (
                        <div>
                            <h1 className="title4 bottomtext2" style={{ top: '-30px' }}>Results for "{searchQuery}"</h1>
                            <h1 className="title1">{searchQuery.title}</h1>
                            <Carousel className="caroborder no-blur" interval={null}>
                                {movieChunks.map((chunk, chunkIndex) => {
                                    return (
                                        <Carousel.Item key={chunkIndex}>
                                            <Row>
                                                {chunk.map((result) => {
                                                    return (
                                                        <Col key={result.id} className="mb-4 no-blur">
                                                            {result.image &&
                                                                <Card>
                                                                    <div className="container">
                                                                        <Card.Img
                                                                            className="movie-card blur"
                                                                            style={{
                                                                                backgroundImage: `${result.image}` ? `url(${result.image.url})` : null,
                                                                            }}
                                                                        />
                                                                    </div>
                                                                    <Card.ImgOverlay>
                                                                        <img
                                                                            src={result.image.url}
                                                                            className="img-thumbnail"
                                                                            alt="movie image"
                                                                            height="600"
                                                                            width="600"
                                                                        />
                                                                        <Card.Title className="titlecard">{result.title}</Card.Title>
                                                                        <Button
                                                                            className="buttondetail"
                                                                            variant="black"
                                                                            size="lg"
                                                                            onClick={() => navigate(`/movie${result.id}`)}
                                                                        >
                                                                            Details
                                                                        </Button>
                                                                    </Card.ImgOverlay>
                                                                </Card>
                                                            }
                                                        </Col>
                                                    );
                                                })}
                                            </Row>
                                        </Carousel.Item>
                                    );
                                })}

                            </Carousel>
                        </div>
                    ) : null}
                    <div className="main main5">
                        <video
                            style={{
                                position: "fixed",
                                zIndex: -1,
                                width: "100%",
                                height: "100%",
                                opacity: 0,
                                animation: "fade-in 1s forwards",
                            }}
                            src={particle}
                            autoPlay
                            loop
                            muted
                        />
                    </div>
                </div>
            )}
        </div>
    );
}
export default SearchBar;
