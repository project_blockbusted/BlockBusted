
import MainPage from './components/MainPage'
import './styles/App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './components/Nav';
import { AuthProvider, useToken } from "./AuthProvider";
import MovieDetail from "./components/MovieDetail"
import MovieList from "./components/MovieList"
import SearchBar from './components/SearchBar';
import Profiles from './components/Profiles';
import Watchlist from './components/WatchList';


function GetToken() {
  useToken();
  return null;
}

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  return (
    <BrowserRouter basename={basename}>
      <AuthProvider>
        <GetToken />
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/movies" element={<MovieList />} />
          <Route path="/movie/title/:movie_id" element={<MovieDetail />} />
          <Route path="/search" element={<SearchBar />} />
          <Route path="/developers" element={<Profiles />} />
          <Route path="/watchlist" element={<Watchlist />} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}
export default App;
