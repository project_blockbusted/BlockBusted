THURS 2/9 Today we wrote out a list of API end points for our new project. The endpoints represented different CRUD methods for the functionality of the website. We also created a git group and forked the boilerplate project (project gamma) into the GitHub repository for our group. Finishing touches were made to the framework design.

FRI 2/10 We now have a better detailed and more accurate API documentation after meeting with Jimmy. We wrote a mock up of some of the functionality of our endpoints with Acceptance Criteria and Definition of Done. We also received a large chuck of Learn documentation and videos that will outline the structure of our application.

MON 2/13 We presented our wireframe of the project to the entire class today. We also dove into the large amount of learn documentation and videos.

TUES 2/14 We had a productive session on connecting our project to the docker framework. We also made progress on authentication, logging in of users, and the structure of the project as a whole.

WED 2/15 We started the day with just swagger working, and by the end of the day we had the authentication portion completed as well as a couple of endpoints.

THURS 2/16 We worked on getting our end points to be in a place where we can leave it for the weekend.

FRI 2/17 DAY OFF

MON 2/20 DAY OFF

TUES 2/21 We got an authenticated user from our database to login and logout and search for a movie successfully using our external API. We also made adjustments to our file structure and YAML file.

WED 2/22 We were able to start to design of our front end. We produced links that will potentially lead to our endpoints. We installed bootstrap and started with our modals.

THURS 2/23 We started actually coding our front end. We added some CSS as well. Lots of work put into front-end authentication with intent to finish it completely tomorrow.

FRI 2/24 We finished with our modal functionality as well as front-end authentication. We had some naming convention errors that held us back but we were able to parse through our code and make the necessary changes. We also had some minor problems with working on different branches but was resolved with little effort.

MON 2/27 We worked on CSS and Front end pretty-fying.

TUES 2/28 Today we were able to resolve some issues that arose from our backend. We are now able to receive information from our third party API (after we paid for it) and we could display the information on the front end. We can display the top 20 movies with their poster.

WED 3/1 Today we coded our front end to show a details of a movie. First we did this via title, then realized using an id will be more efficient. Then we linked the detail page and the list page, which was major because it required tweaking on both the front and back end.

THURS 3/2 Today we got our search functionality working along with a search page and a search bar component. The search turns up results and is linked to the detail page of that movie. Also, much styling was done including a carousel, several nav bar changes and a whole lot of general styling.

FRI 3/3 We made massive strides in styling and organizing our data. We decided to add a major new feature where the user is able to see where the movie is current available to stream.

MON 3/6 Today we went over a lot of the error messages that came up for specific movies. Basically we sorted out our movie edge cases and set conditional statements throughout the project. Additional styling changes were also added to increase GUI functionality.

TUES 3/7 We made try/catch statements with conditionals to sort out errors that we anticipate. We also added the ability to click on a streaming platform (if one is available) and it will take you directly to the streaming website. This was done via the API. More work was done on styling and formatting for aesthetics.

WED 3/8 Today we started with started with flake 8 fixes and we were able to get it down to 2 errors that we were not able to fix. This is because the line is a url that we have to make a request to. We also did more work on authentication so that an alert will pop up in the event that the user inputs an incorrect email opr password and if the email is already assigned in our database. We also eliminated errors that popped up in the console via various troubleshooting methods.

THURS 3/9 Today we continued to fix some white spaces and unnecessary spaces in our code. We also organized oru file structure to have folders containing our components and our styles. Our project is in a state where we can deploy. Hopefully next week. Tomorrow morning we present to Jimmy so this is likely to be the last entry.

FRI 3/10 Just kidding this is the last entry. Today we worked on the docs part of our project, mostly eliminating unused imports and adding additional information to our readme file. We also presented to Jimmy today and that went well. Ok goodbye.
