Journals

2/9 : created wireframing for the project and mapped out the API endpoints for what we are going to create.

2/10 : looked through databases to decide if we are going with mongoDB or postgres and checked with our instructor to make sure our API endpoints are good

2/13 : today we learned more about fast API and the authentication system which we will implement in the project

2/14 : we did the proper docker files so we can docker compose the project properly and got it to work, also touching a bit of the frontend side with authentication, and starting to implement our database.

2/15 : today we got our mongodb database working and did most of the authentication system going.

2/16 : continued to work on authentication.

2/21 : rearranged project directories to make authentication work correctly, but now the end points and authentication work correctly.

2/22 : we started our front end, main mage and nav bar is complete, diving in a bit more into finishing the front end authentication, also created model for the sign in page.

2/23 : getting authentication to work, created home page layout with some bootstrap and css work, added video and made logo clickable.

2/24 : we made our front end authentication work.

2/27 : today we started trying to pull the api from our back end to make the movie list work, made the my account page and changed the home page and nav bar a bit.

2/28 : we made our api work correctly, started to make it work with the back end and now wors in the front end as well.

3/1 : worked on our movie list and movie details trying to debug the error of it not pulling correctly, but got movie details to pull individual components to the unique id.

3/2 : a'lot off css revamping, for the home screen, the movie details and the movie list, added search function and edited nav bar.

3/3 : made the search bar page work correctly with added styling, made an about us page and started implementing our second api.

3/6 : complete css revamp for the whole project, added developers page details and made both apis work together with no issues and added logos for streaming.

3/7 : we tested some errors to see where they are coming from and we made them go away, made sure the whole project flows the way it should should with no errors, also made the streaming links clickable so they will redirect you to the page of the streaming site.

3/8 : today we cleaned up our code to make sure its understandable, we also used flake8 to get rid of unnecessary errors or empty lines and bad imports, we also improved our authentication so that it gives you an error if you already have that e,ail as a user, and an error if your password is incorrect when signing in, additional clean up.

3/9 : today we did more code clean up, also added different animations and css, specifically for the search bar and fonts, changed a couple videos and checked if there are any errors on the logs though each page, everything is in stable working condition.

3/10 : today we finalized the project, fixed up the code a bit more, presented our project to the instructor, and also updated the readme file to show how we got the data and made sure to leave the project in stable condition.