3/9/23
searchbar and font changes, cleaned up more code for sudo code

3/8/23
cleaned up code from front end and did flake8 for backend line clean up.

3/7/23
did readme file for entire project, tired to create a favorites page was unsuccessful.

3/6/23
worked on routes for searchbar, cleaned up movie details and serach pages, updated our about us section , resized images on profiles page

3/3/23
worked on more css, tried to implement a search in nav, group worked on router and acls for new route

3/2/23
worked on movielist , searchbar, ,cleaned up navbar, and made css changes

3/1/23
worked on movielist to link to moviedetail page


2/28/23
worked on refactoring acls and routes ... got swagger working... worked on MovieList and got page to display top 20 movies

2/27/23
worked on navbar and myaccount, movieslist and linked to main

2/24/23
got front end to work with login and signup forms

2/23/23
worked on authenticating front end modals , added video background, and updated models

2/22/23
Started front-end  made mav.js, mainpage.js installed bootstrap and added sign in modal

2/16/23
restructuring accounts to main folder and worked on authentication

2/15/23
worked on docker compose file to get mongo express, worked on our accounts folder added session info to finish authentication

2/14/23
created queries and routers for authentication and got docker containers running and fastapi running

2/13/23
worked on authenticating files through the learn day 4 video


2/10/23
talked with Jimmy as a group to review api documentation


2/9/23
worked with the team to create api documentation and finished up wireframe outline
