2/9/23

Today we worked through the process of setting up a group project in gitlab.  We then developed our documentation for the API. This is also the first day of using a journal for the project.

2/10/23

Today we sorted through different databases that we may be using and the difference between them.  We are leaning more towards MongoDB, for now as it might accommodate our needs a little better than an RDBMS.  We are also working on gaining a better understanding of FastAPI so that we feel better about structuring our API throughout the project.  I am feeling better after working through some of our explorations for the day so far.


2/13/23

Today we went through and got better acquainted with FastAPI.  Since this will be our first project utilizing it, we want to make sure we have a clear plan in determining our approach to handle this project.


2/14/23

Today we took steps to work on getting our authentication handled. We also set up a path to be able to login.  Our path is rough for now, but the primary goal was to see if we could get it to show up in our swagger.  Tomorrow we will be learning more about MongoDB, so we will be focusing a lot more on our database then.

2/15/23

Today we worked through almost all of our authentication within our project.  Users are now able to create an account, login and log out of their account.  They are remembered via a token.  This will allow authentication of users and the ability to keep information separate for each user.

2/16/23

Still continued to work on our authentication and we picked the API that we wanted to use.

2/21/23

We solved pretty much every authentication error plaguing us up to this point and we got our API to be able to pull all needed information.  Also made the files much neater and compacted everything into a monolith file instead of having two microservice files.

2/22/23

Today we worked on getting our login modul set up and a basic layout for our homepage and our navbar.  We also installed bootstrap to help with our design process and a few other steps like adding the react dom router.

2/23/23

Today we worked on front end authentication for pretty much the entire day.  We had a route issue we were able to work out we also had to modify our yaml a little bit.  We have a few errors we should be able to work out in the morning hopefully rather quickly.

2/24/23

We worked all day on our front-end login.  This was finally resolved and we had some naming issues.  We also worked through a few problems pertaining to branch merges.

2/27/23

Today we worked a lot on some formatting issues that were in the way of working in our front-end interface.  We also worked on two files to help us to display our movies.  We were not able to complete either of them, but we are much closer now, and we are working on making sure we are pulling from our api from the back-end and not the front-end.

2/28/23

Today we worked a lot on our back-end and pretty much completed most of the heavy lifting from the backend.  We are now also able to show a movie list page and navigate to it from the navbar.

3/1/23

Today we sorted out a detail page.  We can now look at details of an individual movie.  We are having a tough time sorting out how to navigate to the detail page through a click within the movie list page.  We will solve most of this tomorrow and hopefully work towards implementing a search function.  Overall, I feel pretty good about what we were able to accomplish today.

3/2/23

Today we focused on making a search bar work properly and gave it a dedicated page.  It has full functionality and we were also smoothed out a few small bugs.

3/3/23

Today a lot of css was done and we are also in the process of trying to pull more data from a second API and we have been working to see if we can get it to integrate properly with our existing API.  This has been quite the struggle so far but we are able to access the data of the second API, we just need to be able to properly integrate it.

3/6/23

Today we smoothed out a few error messages that we were getting from the second API.  That was the main thing that we did and we also did some styling stuff as well.

3/7/23

Today we sorted out all necessary functionality for our project.  We also did a bit of styling stuff.  Our project is now almost completely error free.

3/8/23

Today we started by reformatting our project and cleaning out dead code and comments.  We also went through flake8 and made sure everything was formatted correctly.  We fixed some functionality with errors that pertained to the login and sign up moduls as well.

3/9/23

As of today our project is pretty much done for the time being.  We did some slight polish and fine tuning.

3/10/23

Today we finished up our readme and made sure everything was working and that we did not find any errors.  We are confident in what we were able to do and really enjoyed working on this project.

