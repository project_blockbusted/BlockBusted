API Design



### GET  Account Information

* Endpoint path: /accounts
* Endpoint method: GET, POST, PUT, DELETE


* Request body:
    ```json
{
  "email": str,
  "password": str,
  "username": str,
  “history”: list,

}

  ```

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```




### Create Signup

* Endpoint path: /signup
* Endpoint method: POST

{
  "email": str,
  "password": str,
  "username": str,

}

  ```

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```


### GET a movie searched

* Endpoint path: /movie
* Endpoint method: GET
* Headers:
  * Authorization: Bearer token

* Request body:
    ```json
{
  "movie_title": str,
  "image_url": str,
  "genre": str,
  "release_date": str,
  "rating": str,
  "synopsis": str,
  "tconst": str,

}

  ```



### GET homepage

* Endpoint path: /home
* Endpoint method: GET
* Headers:
  * Authorization: Bearer token

* Request body:
    ```json
{
  "movie_title": str,
  "image_url": str,
}

  ```


### POST Log in

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
           "username": string,
    	    "password": string,
      },
      "token": string
    }
    ```


### DELETE Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```
