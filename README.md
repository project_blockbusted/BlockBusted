Welcome to BlockBusted

The premier movie search service!

## Team Members

- Anton DeCesare
- Celeste Villar
- Lyndon Hanson
- Yannis Pashalidis

## Design

[API Design](/docs/API_Documentation.md)

[WireFrame Models](/docs/Wireframe.png)


## Intended Market

Any person who wants to know details about a particular movie and where to stream it!

## Functionality

- Users must create an account, which will give them access to our website.
- Users of the site can take a look around search for movies.
- They can also view our movie list for the top movies.
- The user can also view details of a movie to see details of the movie such as year released, genre, imdb rating, run time, plot summary, and where to stream it.

## Stretch Goal functionality

- Considering the implementation of another third party api to add movie trailers to the move detail page

## Project Initialization

    -Requires Docker to be installed

### To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run `docker volume create mongo-data`
4. Run `docker compose build`
5. Run `docker compose up`
6. View on localhost:3000

## Tests

- [Test Get Movie By ID](/api/tests/test_get_movie_by_id.py) / Celeste Villar
- [Test Get Movie Details](/api/tests/test_get_movie_details.py) / Yannis Pashalidis
- [Test Get Movie](/api/tests/test_get_movie.py) / Anton DeCesare
- [Test Search Movie](/api/tests/test_search_movie.py) / Lyndon Hanson

## How the API integrates:

We used two APIs in this project:
1. https://rapidapi.com/apidojo/api/online-movie-database
- This API provides us with all of our titles, images and other information about all movies, TV shows and videos games.
2. https://rapidapi.com/movie-of-the-night-movie-of-the-night-default/api/streaming-availability
- This API gave us the information about where all of the entertainment on our platform can be found to stream and gave us the ability to link to those sites.
- For each movie found, it pulls data from both API sources to output the information.

## Database

- Our database is used to store information about our users and their accounts.

Account information:
    Username: string (required)
    Email: string (required, unique, identifier)
    Password: string (required)

- We have a function in [queries/accounts.py] that is responsible for turning a users password into a hashed password for an extra layer of account protection.

- We also use our database to store the ids for movies to be able to reference and use data from.  This id is then used with our API's, in order to populate the page with the same information as the other pages.  This is done to maintain consistency throughout the project.
